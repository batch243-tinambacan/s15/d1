// console.log("Hello World");

// [Section] Syntax, Statements and comments

let myVariable="Ada Lovelace";
console.log(myVariable);

let productName="Desktop Computer"
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.59;
console.log(interest);

productName = "Laptop";
console.log(productName);
console.log(productName);

// interest = 4.489
// console.log(interest)

a=5;
console.log(a)
var a;

let outerVar = "hello from the other side";

{
	let innerVar = "hello from the block";
	console.log(innerVar);
	console.log(outerVar);
}

{
	let innerVar = "hello from the second block";
	console.log(innerVar);
	let outerVar = "Hello from the second side"

}

console.log(outerVar);
// console.log(innerVar);

let productCode = "DC017" , productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// Reserved keyword will cause an error
// const let = "hello";
// console.log(let);

let country = "Philippines";
let province = 'Metro Manila';

let fullAddress = province + ", "+ country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape characters (\)
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early"
message = 'John\'s employees went home early'
console.log(message)

// Numbers - Integers
let count = "26"; 
let headcount = 26;
console.log(headcount)
console.log(count)

// Numbers - Decimal

let grade = 98.7;
console.log(grade)

// Numbers-exponential
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);
console.log(count + headcount);

// Boolean
let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);
console.log("isMarried" + isMarried);

// Arrays
	// Similar data types
	let grades=[98.7, 82.1, 90.2, 94.6];
	console.log(grades);
	console.log(grades[1]);

	// different data types; not recommended
	let details = ["John", 32, "Smith", true];
	console.log(details);

// Objects
let person={
	fullname: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0914124666", "8123 4567"],
	address: {
			houseNumber: '345',
			city: "Manila",
	}

};
console.log(person)

// Type of operator

console.log(person);
console.log(person.contact[1]);
console.log(person.address.city);